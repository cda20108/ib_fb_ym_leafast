package fr.afpa.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Livreur;

public interface LivreurRepository extends JpaRepository<Livreur, Integer>{

}
