package fr.afpa.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.admin;

public interface AdminRepository extends JpaRepository<admin, Integer>{

	
}
