package fr.afpa.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Annonce;

public interface AnnonceRepository extends JpaRepository<Annonce, Integer>{

}
