package fr.afpa.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Particulier;

public interface ParticulierRepository extends JpaRepository<Particulier, Integer> {

}
