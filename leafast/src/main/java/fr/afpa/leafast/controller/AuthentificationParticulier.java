package fr.afpa.leafast.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import fr.afpa.beans.Particulier;
import fr.afpa.repositories.dao.ParticulierRepository;

@Controller
@SessionAttributes("particulier")
public class AuthentificationParticulier {

	@Autowired
	ParticulierRepository pr;

	@GetMapping(value = "/singin")
	public String singIn(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		ArrayList<Particulier> listeParticulier = (ArrayList<Particulier>) pr.findAll();

		if (listeParticulier != null) {
			for (Particulier particulier : listeParticulier) {
				if (particulier.getCompte().getLogin().equals(login) && particulier.getCompte().getMdp().equals(mdp)) {
					model.addAttribute("particulier", particulier);
					return "menuParticulier";
				}
			}
		}

		return "home";

	}

	@GetMapping(value = "/singout")
	public String singOut(HttpSession session, SessionStatus status) {

		session.removeAttribute("particulier");
		status.setComplete();

		return "redirect:/";
	}
}
