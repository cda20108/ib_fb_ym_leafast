package fr.afpa.leafast.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Particulier;
import fr.afpa.controlSaisie.ControlSaisieParticulier;
import fr.afpa.repositories.dao.ParticulierRepository;

@Controller

public class ParticulierController {

	@Autowired
	ParticulierRepository pr;

	ControlSaisieParticulier csp = new ControlSaisieParticulier();

	@GetMapping(value = "/inscription")
	public String redirectForm() {
		return "inscription";
	}

	@PostMapping(value = "/inscription")
	public ModelAndView ajoutPersonne(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "mail") String mail,
			@RequestParam(value = "tel") String tel, @RequestParam(value = "date") String date,
			@RequestParam(value = "numRue") String numRue, @RequestParam(value = "nomRue") String nomRue,
			@RequestParam(value = "codeP") String codeP, @RequestParam(value = "ville") String ville,
			@RequestParam(value = "login") String login, @RequestParam(value = "mdp") String mdp) {

		LocalDate dateNaissance = LocalDate.parse(date);

		if (csp.controlNom(nom) == false || csp.controlPrenom(prenom) == false || csp.controlMail(mail) == false) {
			System.out.println("1");
			mv.setViewName("inscriptionRessai");
			return mv;
		}

		Adresse adresse = new Adresse(numRue, nomRue, codeP, ville);

		Compte compte = new Compte(login, mdp);

		Particulier par = new Particulier(nom, prenom, mail, tel, dateNaissance);

		par.setAdresse(adresse);
		par.setCompte(compte);

		pr.save(par);

		pr.flush();

		mv.addObject("particulier", par);

		mv.setViewName("home");

		return mv;
	}

	@GetMapping(value = "/modifierParticulier")
	public ModelAndView modifierParticulier(ModelAndView mv, @RequestParam(value = "id") Integer id) {

		Particulier particulier = new Particulier();

		particulier = pr.findById(id).get();

		mv.addObject("particulier", particulier);

		mv.setViewName("modifierInfosParticulier");

		return mv;
	}

	@PostMapping(value = "/modifierParticulier")
	public ModelAndView modifierParticulier(ModelAndView mv, @RequestParam(value = "idP") Integer idP,
			@RequestParam(value = "nom") String nom, @RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "mail") String mail, @RequestParam(value = "tel") String tel,
			@RequestParam(value = "date") String date, @RequestParam(value = "idA") Integer idA,
			@RequestParam(value = "numRue") String numRue, @RequestParam(value = "nomRue") String nomRue,
			@RequestParam(value = "codeP") String codeP, @RequestParam(value = "ville") String ville,
			@RequestParam(value = "idC") Integer idC, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		LocalDate dateNaissance = LocalDate.parse(date);

		Adresse adresse = new Adresse();

		Compte compte = new Compte();

		adresse.setIdAdresse(idA);
		adresse.setNumRue(numRue);
		adresse.setNomRue(nomRue);
		adresse.setCodePostal(codeP);
		adresse.setVille(ville);

		compte.setIdCompte(idC);
		compte.setLogin(login);
		compte.setMdp(mdp);

		Particulier particulier = new Particulier();

		particulier.setIdParticulier(idP);
		particulier.setNom(nom);
		particulier.setPrenom(prenom);
		particulier.setMail(mail);
		particulier.setTel(tel);
		particulier.setDateNaissance(dateNaissance);
		particulier.setAdresse(adresse);
		particulier.setCompte(compte);

		pr.save(particulier);

		mv.addObject("particulier", particulier);

		mv.setViewName("menuParticulier");

		return mv;
	}

	@GetMapping(value = "/supprimerParticulier")
	public ModelAndView supprimerParticulier(ModelAndView mv, @RequestParam(value = "id") Integer id) {

		pr.deleteById(id);

		mv.setViewName("home");

		return mv;
	}

	@GetMapping(value = "/listeParticuliers")
	public ModelAndView listeParticuliers(ModelAndView mv, WebRequest request) {

		Particulier parSession = (Particulier) request.getAttribute("particulier", WebRequest.SCOPE_SESSION);

		if (parSession != null) {

			ArrayList<Particulier> liste = (ArrayList<Particulier>) pr.findAll();

			mv.addObject("listepar", liste);

			mv.setViewName("listeParticuliers");
		} else {
			mv.setViewName("home");
		}

		return mv;
	}
}
