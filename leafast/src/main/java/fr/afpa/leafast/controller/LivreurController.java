package fr.afpa.leafast.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import fr.afpa.beans.Compte;
import fr.afpa.beans.Vehicule;
import fr.afpa.beans.Livreur;

import fr.afpa.repositories.dao.LivreurRepository;

@Controller
public class LivreurController {

	@Autowired
	LivreurRepository lr;

	@GetMapping(value = "/inscription2")
	public String redirectForm() {
		return "inscriptionLivreur";
	}
	@GetMapping(value = "/ajouterVehicule")
	public String redirectVForm() {
		return "ajoutVehicule";
	}
	@GetMapping(value = "/connexionL")
	public String redirectCoForm() {
		return "ConnexionLivreur";
	}

	@PostMapping(value = "/inscriptionL")
	public ModelAndView ajoutLivreur(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "mail") String mail,
			@RequestParam(value = "tel") String tel, @RequestParam(value = "date") String date,
			@RequestParam(value = "login") String login, @RequestParam(value = "mdp") String mdp) {

		LocalDate dateNaissance = LocalDate.parse(date);

		Livreur livreur = new Livreur(nom,prenom,mail,dateNaissance);

		Compte compte = new Compte(login, mdp);

	

		
		livreur.setCompte(compte);

		lr.save(livreur);

		lr.flush();

		mv.addObject("livreur",livreur);

		mv.setViewName("home");

		return mv;
	}
	@GetMapping(value = "/modifierLivreur")
	public ModelAndView modifierParticulier(ModelAndView mv, @RequestParam(value = "id") Integer id) {

		Livreur livreur = new Livreur();

		livreur = lr.findById(id).get();

		mv.addObject("livreur", livreur);

		mv.setViewName("modifierInfosParticulier");

		return mv;
	}

	@PostMapping(value = "/modifierLivreur")
	public ModelAndView modifierParticulier(ModelAndView mv, @RequestParam(value = "idL") Integer idL,
			@RequestParam(value = "idC") Integer idC,
			@RequestParam(value = "nom") String nom, @RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "mail") String mail,
			@RequestParam(value = "date") String date,@RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		LocalDate dateNaissance = LocalDate.parse(date);

		

		Compte compte = new Compte();

	

		compte.setIdCompte(idC);
		compte.setLogin(login);
		compte.setMdp(mdp);

		Livreur livreur = new Livreur();

		livreur.setIdLivreur(idL);
		livreur.setNom(nom);
		livreur.setPrenom(prenom);
		livreur.setEmail(mail);
	
		livreur.setDate(dateNaissance);
		
		livreur.setCompte(compte);

		lr.save(livreur);

		mv.addObject("livreur", livreur);

		mv.setViewName("menuLivreur");

		return mv;
	}

	@GetMapping(value = "/supprimerLivreur")
	public ModelAndView supprimerParticulier(ModelAndView mv, @RequestParam(value = "id") Integer id) {

		lr.deleteById(id);

		mv.setViewName("home");

		return mv;
	}
	
	@PostMapping(value = "/ajouterVehicule")
	public ModelAndView modifierParticulier(ModelAndView mv,
			@RequestParam(value = "typeVehicule") String type, @RequestParam(value = "marqueVehciule") String marque,
			@RequestParam(value = "modelVehicule") String model,@RequestParam(value = "emission") Integer emission,
			@RequestParam(value = "plaque") String plaque)
			 {

		
		Vehicule vehicule = new Vehicule(type,marque,model,emission,plaque);
		

		Compte compte = new Compte();



		Livreur livreur = new Livreur();
		
		livreur.setVehicule(vehicule);
		

		lr.save(livreur);

		mv.addObject("livreur", livreur);

		mv.setViewName("menuLivreur");

		return mv;
	}
}
