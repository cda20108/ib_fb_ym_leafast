package fr.afpa.leafast.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Annonce;
import fr.afpa.repositories.dao.AnnonceRepository;

@Controller
public class AnnonceController {

	@Autowired
	AnnonceRepository ap;
	
	@GetMapping(value = "/creationAnnonce")
	public String redirectForm() {
		return "creationAnnonce";
	}
	
	@PostMapping(value = "/creationAnnonce")
	public ModelAndView creerAnnonce(ModelAndView mv, @RequestParam(value = "nom_annonce") String nom_annonce,
			@RequestParam(value = "ville_annonce") String ville_annonce, @RequestParam(value = "taille_colis") String taille_colis,
			@RequestParam(value = "prix_km") String prix_km, @RequestParam(value = "date_limite") String date_limite
			, @RequestParam(value = "descriptif") String descriptif) {

		LocalDate date = LocalDate.parse(date_limite);

		Annonce annonce = new Annonce(nom_annonce, ville_annonce, taille_colis, prix_km, prix_km, date, descriptif);

		System.out.println(annonce);

		ap.save(annonce);

		ap.flush();

		mv.addObject("annonce", annonce);

		mv.setViewName("home");

		return mv;
	}
	
	@PostMapping(value = "/modifAnnonce")
	public ModelAndView modifierAnnonce (ModelAndView mv, @RequestParam(value = "nom_annonce") String nom_annonce,
	@RequestParam(value = "ville_annonce") String ville_annonce, @RequestParam(value = "taille_colis") String taille_colis,
	@RequestParam(value = "prix_km") String prix_km, @RequestParam(value = "date_limite") String date_limite
	, @RequestParam(value = "descriptif") String descriptif) {
		
		LocalDate date = LocalDate.parse(date_limite);
		
		Annonce annonce = new Annonce(nom_annonce, ville_annonce, taille_colis, prix_km, prix_km, date, descriptif);

		ap.saveAndFlush(annonce);
		
		

		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();

		for (Annonce ann : liste) {

			System.out.println(ann);

		}

		mv.addObject("listeAnn", liste);
		
		mv.setViewName("listeAnnonce");
		
		return mv;
	}
	
	@GetMapping(value = "/suppressionAnnonce")
	public ModelAndView supprimerAnnonce(ModelAndView mv, @RequestParam(value = "id") String id) {
		
		
		ap.deleteById(Integer.parseInt(id));
		
		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();

		for (Annonce ann : liste) {

			System.out.println(ann);

		}

		mv.addObject("listeAnn", liste);
		
		mv.setViewName("listeAnnonce");
		
		return mv;
	}
	
	
	@GetMapping(value = "/listerAnnonce")
	public ModelAndView listeAnnonce(ModelAndView mv) {

		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();

		for (Annonce ann : liste) {

			System.out.println(ann);

		}

		mv.addObject("listeAnn", liste);

		mv.setViewName("listeAnnonce");

		return mv;
	}
	
}
