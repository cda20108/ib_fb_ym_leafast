package fr.afpa.leafast.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import fr.afpa.beans.Livreur;
import fr.afpa.repositories.dao.LivreurRepository;

@Controller
@SessionAttributes("livreur")
public class AuthentificationLivreur {
	@Autowired
	LivreurRepository lr;

	@GetMapping(value = "/singin2")
	public String singIn(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		ArrayList<Livreur> listeLivreur = (ArrayList<Livreur>) lr.findAll();

		if (listeLivreur != null) {
			for (Livreur livreur : listeLivreur) {
				if (livreur.getCompte().getLogin().equals(login) && livreur.getCompte().getMdp().equals(mdp)) {
					model.addAttribute("livreur", livreur);
					return "menuLivreur";
				}
			}
		}

		return "home";

	}

	@GetMapping(value = "/singout2")
	public String singOut(HttpSession session, SessionStatus status) {

		session.removeAttribute("livreur");
		status.setComplete();

		return "redirect:/";
	}

}
