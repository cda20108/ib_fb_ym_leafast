package fr.afpa.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Adresse {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idAdresse;

	@NonNull
	private String numRue;

	@NonNull
	private String nomRue;

	@NonNull
	private String codePostal;

	@NonNull
	private String ville;
	
	@OneToOne(mappedBy = "adresse")
	private Particulier particulier;

}
