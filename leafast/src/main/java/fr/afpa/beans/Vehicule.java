package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
public class Vehicule {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_vehicule")
	private Integer idVehicule;
	@NonNull
	@Column(nullable = false)
	private String typeVehicule;
	@NonNull
	@Column(nullable = false)
	private String marqueVehciule;
	@NonNull
	@Column(nullable = false)
	private String modelVehicule;
	@NonNull
	@Column(nullable = false)
	private Integer emission;
	@NonNull
	@Column(nullable = false)
	private String plaque;
	@OneToOne(mappedBy = "vehicule")
	private Livreur livreur;
	
}
