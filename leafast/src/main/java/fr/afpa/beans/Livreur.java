package fr.afpa.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
public class Livreur {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idLivreur;
	@NonNull
	private String nom;
	@NonNull
	private String prenom;
	@NonNull
	private String email;
	@NonNull
	private LocalDate date;
	
	//@OneToMany(mappedBy = "livreur")
	//private List<Annonce> listeAnnonce;
	
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_vehicule")
	private Vehicule vehicule;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_compte")
	private Compte compte;

}
