package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Annonce {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idAnnonce;

	@NonNull
	private String nom_annonce;

	@NonNull
	private String ville;

	@NonNull
	private String taille_colis;

	@NonNull
	private String prix_km;

	@NonNull
	private String prix_total;
	
	@NonNull
	private LocalDate date_limite;
	@NonNull
	private String descriptif;

}
