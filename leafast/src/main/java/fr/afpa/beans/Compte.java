package fr.afpa.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Compte {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCompte;

	@NonNull
	private String login;

	@NonNull
	private String mdp;

	@OneToOne(mappedBy = "compte")
	private Particulier particulier;

}
