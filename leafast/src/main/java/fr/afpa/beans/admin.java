package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class admin {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idParticulier;

	@NonNull
	private String nom;

	@NonNull
	private String prenom;

	@NonNull
	private String mail;

	@NonNull
	private String tel;

	@NonNull
	private LocalDate dateNaissance;
}
