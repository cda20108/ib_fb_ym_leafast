package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Particulier {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idParticulier;

	@NonNull
	private String nom;

	@NonNull
	private String prenom;

	@NonNull
	private String mail;

	@NonNull
	private String tel;

	@NonNull
	private LocalDate dateNaissance;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_adresse")
	private Adresse adresse;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_compte")
	private Compte compte;

}
