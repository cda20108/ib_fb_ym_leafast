package fr.afpa.controlSaisie;

public class ControlSaisieParticulier {

	public boolean chaineAlphabetique(String nom) {

		for (int i = 0; i < nom.length(); i++) {

			if (!(Character.isAlphabetic(nom.charAt(i)))) {
				return false;
			}
		}
		return true;
	}

	public boolean controlNom(String nom) {

		if (nom.length() < 40 && chaineAlphabetique(nom) == true) {

			return true;
		}

		else {
			return false;
		}

	}

	public boolean controlPrenom(String prenom) {

		if (prenom.length() < 30 && chaineAlphabetique(prenom) == true) {

			return true;
		}

		else {
			return false;
		}
	}

	public boolean controlMail(String mail) {

		if ((mail.matches(".+@.+\\.[a-z]+")) == true) {

			return true;
		} else {
			return false;
		}
	}
}
