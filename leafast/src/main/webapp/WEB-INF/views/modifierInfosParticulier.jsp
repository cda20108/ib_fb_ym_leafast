<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifier ses informations</title>
</head>
<body>

	<form action="modifierParticulier" method="post">
	
		<input type="text" value="${ particulier.idParticulier }" name="idP" hidden="true">

		<input type="text" value="${ particulier.nom }" name="nom">
		<input type="text" value="${ particulier.prenom }" name="prenom">
		<input type="text" value="${ particulier.mail }" name="mail">
		<input type="text" value="${ particulier.tel }" name="tel">
		<input type="date" value="${ particulier.dateNaissance }" name="date">
		
		<input type="text" value="${ particulier.adresse.idAdresse }" name="idA" hidden="true">
		
		<input type="text" value="${ particulier.adresse.numRue }" name="numRue">
		<input type="text" value="${ particulier.adresse.nomRue }" name="nomRue">
		<input type="text" value="${ particulier.adresse.codePostal }" name="codeP">
		<input type="text" value="${ particulier.adresse.ville }" name="ville">
		
		<input type="text" value="${ particulier.compte.idCompte }" name="idC" hidden="true">
		
		<input type="text" value="${ particulier.compte.login }" name="login" readonly>
		<input type="password" value="${ particulier.compte.login }" name="mdp">

		<button type="submit">VALIDER</button>

	</form>
	
	<a href="singout">SE DECONNECTER</a>

</body>
</html>