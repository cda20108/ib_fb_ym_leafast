<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste des particuliers</title>
</head>
<body>

	<c:if test="${ !empty particulier }">

		<h1>Liste des particuliers</h1>

		<table>

			<tr id="haut">
				<td>ID</td>
				<td>Nom</td>
				<td>Prenom</td>
				<td>Login</td>
			</tr>

			<c:forEach items="${ listepar }" var="par">

				<tr>
					<td>${ par.idParticulier }</td>
					<td>${ par.nom }</td>
					<td>${ par.prenom }</td>
					<td>${ par.compte.login }</td>
					<td><a href="modifierParticulier?id=${ par.idParticulier }">MODIFIER</a></td>
					<td><a href="supprimerParticulier?id=${par.idParticulier }">SUPPRIMER</a></td>
				</tr>

			</c:forEach>

		</table>

		<br>

	</c:if>

	<a href="singout">SE DECONNECTER</a>

</body>
</html>