<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/animate.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<!-- Google Fonts -->
<link
	href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900'
	rel='stylesheet' type='text/css'>



<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<meta charset="ISO-8859-1">
<title>Inscription</title>
</head>
<body>
	<form action="inscription" method="post">
		<div class="container">
			<div class="top">
				<div class="logo">
					<img src=" ../webapp/resources/images/logo_transparent.png"
						height=150 weight=150 alt="">
				</div>
				<h1 id="title" class="hidden">
					<span id="logo"><span>Une livraison plus sur pour
							vous et l'environnement</span></span>
				</h1>
			</div>
			<div class="login-box animated fadeInUp">
				<div class="box-header">
					<h2>Inscription particulier</h2>
				</div>
				<label for="username">Nom</label> <br /> <input type="text"
					placeholder="Nom" name="nom"> <br />
			
			<label for="username">Prenom</label> <br /> <input type="text"
				placeholder="Prenom" name="prenom"> <br />
		
		<label for="username">E-mail</label> <br /> <input type="text"
			placeholder="Mail" name="mail"> <br />
		
		<label for="username">Telephone/label> <br /> <input type="text"
			placeholder="T�l" name="tel"> <br />
		
		<label for="username">date de naissance</label> <br /> <input type="date"
			placeholder="Date de naissance" name="date"> <br />
		
		<label for="username">Numero de rue</label> <br /> <input type="text"
			placeholder="Num�ro de rue" name="numRue"> <br />
		
		<label for="username">Nom de rue</label> <br /> <input type="text"
			placeholder="Nom de rue" name="nomRue"> <br />
		
		<label for="username">Code Postale</label> <br /> <input type="text"
			placeholder="Code postal" name="codeP"> <br />
		
		<label for="username">Ville</label> <br /> <input type="text"
			placeholder="Ville" name="ville"> <br />
		
		<label for="username">Login de connexion</label> <br /> <input type="text"
			placeholder="Login" name="login"> <br />
		<label for="username">Mot de passe</label> <br /> <input type="password"
			placeholder="Mot de passe" name="mdp"> <br />

		<button type="submit">VALIDER</button>

	</form>
	</div>
	</div>
</body>
<script>
	$(document).ready(function() {
		$('#logo').addClass('animated fadeInDown');
		$("input:text:visible:first").focus();
	});
	$('#username').focus(function() {
		$('label[for="username"]').addClass('selected');
	});
	$('#username').blur(function() {
		$('label[for="username"]').removeClass('selected');
	});
	$('#password').focus(function() {
		$('label[for="password"]').addClass('selected');
	});
	$('#password').blur(function() {
		$('label[for="password"]').removeClass('selected');
	});
</script>

</html>